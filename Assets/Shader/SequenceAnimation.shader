Shader "Unlit/SequenceAnimation"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}//贴图
        _Color("Color", Color) = (1, 1, 1, 1)   // 颜色
         _HorizontalAmount("Horizontal Amount", float) = 8 // 行数
         _VerticalAmount("Vertical Amount", float) = 8  // 列数
         _Speed("Speed", Range(1, 100)) = 30 // 播放速度
    }
    SubShader
    {
//        由于序列帧图像通常包含了透明通道，因此可以被当成是一个半透明对象。
//        在这里我们使用半透明的“标配”来设置它的SubShader标签，即把Queue和RenderType设置成Transparent，把IgnoreProjector设置为True
       Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"  }
        LOD 100

        Pass
        {
//            使用Blend命令来开启并设置混合模式，同时关闭了深度写入
            Tags{"LightMode"="ForwardBase"}
             ZWrite Off
             Blend SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

           
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            float _HorizontalAmount;
            float _VerticalAmount;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float time = floor(_Time.y*_Speed);
                float y = floor(time/_HorizontalAmount);
                float x = time-y*_VerticalAmount;
                half2 uv = i.uv+half2(x,-y);
                uv.x /= _HorizontalAmount;
                uv.y/=_VerticalAmount;
                fixed4 col = tex2D(_MainTex, uv);
                return col;
            }
            ENDCG
        }
    }
          FallBack  "Transparent/VertexLit"
}
