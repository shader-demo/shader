// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "My/tietu2"
{
    Properties
    {
        _MainTex("Main Tex",2D) = "white"{}
        _NormalMap("Normal Map",2D) = "bump"{}
        _Range("Range",Range(0,1)) = 0.5
    }
    SubShader
    {
       Tags{"LightMode" = "ForwardBase" }
        

        Pass{
            CGPROGRAM
            #include "Lighting.cginc"
       
            #pragma vertex vert;
            #pragma fragment frag;
            float _Range;
            sampler2D _MainTex;
    
            float4 _MainTex_ST;
            sampler2D _NormalMap;
            float4 _NormalMap_ST;
            struct a2v
            {
                float4 vertex:POSITION;
                float4 texcoord:TEXCOORD0;
                float3 normal:NORMAL;
                float4 tangent:TANGENT;
            };

            struct v2f
            {
                 float4 uv:TEXCOORD0;
                 float4 svPos:SV_POSITION;
                 float3 normal:TEXCOORD1;
                 float3 lightDir:TEXCOORD2;
            };


            v2f vert(a2v v)
            {
               v2f f;
                f.svPos = UnityObjectToClipPos(v.vertex);
                f.uv.xy = v.texcoord.xy*_MainTex_ST.xy+_MainTex_ST.zw;
                f.uv.zw = v.texcoord.xy*_NormalMap_ST.xy+_NormalMap_ST.zw;;
                // f.normal = UnityObjectToWorldNormal(v.normal);
                TANGENT_SPACE_ROTATION;//调用之后，会得到一个矩阵rotation，这个矩阵用来把模型空间下的方向转换为切线空间
                f.lightDir = mul(rotation,ObjSpaceLightDir(v.vertex));//把光从模型空间，转为切线空间
                return f;
            }
            fixed4 frag(v2f f):SV_Target{
                fixed3 texColor = tex2D(_MainTex,f.uv.xy);
                //方式一：normalDir通过noramlColor计算，noramlColor范围0~1，乘以2再减一转为-1~1，即法线的范围
                // fixed3 noramlColor = tex2D(_NormalMap,f.uv.zw);
                // fixed3 normalDir = normalize(noramlColor*2-1);
                //方式二：normalDir通过内部函数UnpackNormal计算得来
                half4 noramlColor = tex2D(_NormalMap,f.uv.zw);
                fixed3 normalDir = UnpackNormal(noramlColor);
                // normalDir = normalize(normalDir);
                fixed3 lightDir = normalize(f.lightDir);
                fixed3 texColo =  _LightColor0.rgb*texColor*max(0,dot(normalDir,lightDir)*0.5+0.5);
                fixed3 color = texColo+UNITY_LIGHTMODEL_AMBIENT.rgb;
                return fixed4(color,1);
            }

         
            ENDCG
        }
     
    }
    FallBack "Diffuse"
}
