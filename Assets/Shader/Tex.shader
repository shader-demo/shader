// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "My/tietu"
{
    Properties
    {
        _MainTex("Main Tex",2D) = "white"{}
        _Range("Range",Range(0,1)) = 0.5
    }
    SubShader
    {
       Tags{"LightMode" = "ForwardBase" }
        

        Pass{
            CGPROGRAM
            #include "Lighting.cginc"
            sampler2D _MainTex;
            #pragma vertex vert;
            #pragma fragment frag;
            float _Range;
            struct a2v
            {
                float4 vertex:POSITION;
                float4 texcoord:TEXCOORD0;
                float3 normal:NORMAL;
            };

            struct v2f
            {
                 float4 uv:TEXCOORD0;
                 float4 svPos:SV_POSITION;
                 float3 normal:TEXCOORD1;
            };
            v2f vert(a2v v)
            {
               v2f f;
                f.svPos = UnityObjectToClipPos(v.vertex);
                f.uv = v.texcoord;
                f.normal = UnityObjectToWorldNormal(v.normal);
                return f;
            }
            fixed4 frag(v2f f):SV_Target{
                fixed3 texColor = tex2D(_MainTex,f.uv.xy);
                fixed3 normalDir = normalize(f.normal);
                fixed3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
                fixed3 texColo =  _LightColor0.rgb*_Range*texColor*max(0,dot(normalDir,lightDir)*0.5+0.5);
                fixed3 color = texColo+UNITY_LIGHTMODEL_AMBIENT.rgb;
                return fixed4(color,1);
            }

         
            ENDCG
        }
     
    }
    FallBack "Diffuse"
}
