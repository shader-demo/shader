// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "My/tietu3"
{
    Properties
    {
        _MainTex("Main Tex",2D) = "white"{}
        _NormalMap("Normal Map",2D) = "bump"{}
        _BumpScale("Bump Scale",Float) =1
    }
    SubShader
    {
       Tags{"LightMode" = "ForwardBase" }
        

        Pass{
            CGPROGRAM
            #pragma enable_d3d11_debug_symbols
            #include "Lighting.cginc"
       
            #pragma vertex vert;
            #pragma fragment frag;
            sampler2D _MainTex;
    
            float4 _MainTex_ST;
            sampler2D _NormalMap;
            float4 _NormalMap_ST;
            float _BumpScale;
            struct a2v
            {
                float4 vertex:POSITION;
                float4 texcoord:TEXCOORD0;
                float3 normal:NORMAL;
                float4 tangent:TANGENT;
            };

            struct v2f
            {
                 float4 uv:TEXCOORD0;
                 float4 svPos:SV_POSITION;
                 float3 normal:TEXCOORD1;
                 float3 lightDir:TEXCOORD2;
            };


            v2f vert(a2v v)
            {
               v2f f;
                f.svPos = UnityObjectToClipPos(v.vertex);
                f.uv.xy = v.texcoord.xy*_MainTex_ST.xy+_MainTex_ST.zw;
                f.uv.zw = v.texcoord.xy*_NormalMap_ST.xy+_NormalMap_ST.zw;;
                // f.normal = UnityObjectToWorldNormal(v.normal);
                TANGENT_SPACE_ROTATION;//调用之后，会得到一个矩阵rotation，这个矩阵用来把模型空间下的方向转换为切线空间
                f.lightDir = mul(rotation,ObjSpaceLightDir(v.vertex));//把光从模型空间，转为切线空间
                return f;
            }
            fixed4 frag(v2f f):SV_Target{
                fixed3 texColor = tex2D(_MainTex,f.uv.xy);
                //方式一
                // half4 noramlColor = tex2D(_NormalMap,f.uv.zw);
                //
                // half3 normalDir = noramlColor.xyz*2-1;
                // normalDir.xy = normalDir.xy*_BumpScale;
                // normalDir = normalize(normalDir);
                //方式二 
                // half4 noramlColor = tex2D(_NormalMap,f.uv.zw);
                // half3 normalDir;
                // noramlColor.x *= noramlColor.w;
                // normalDir.xy = (noramlColor.xy * 2 - 1)*_BumpScale;
                // normalDir.z = sqrt(1 - saturate(dot(normalDir.xy, normalDir.xy)));
                // normalDir = normalize(normalDir);
                //方式三
                half4 noramlColor = tex2D(_NormalMap,f.uv.zw);
                fixed3 normalDir = UnpackNormal(noramlColor);
                normalDir.xy = normalDir.xy*_BumpScale;
                //用的是切线空间下的法线纹理，因此法线的z为正数 其实(dot(xy,xy))=x*x+y*y
		     	//由于偏移后的法线是归一化的，因此满足x2 + y2 + z2 = 1
			    //所以z=sqrt(1-(x2+y2))
                normalDir.z = sqrt(1.0 - saturate(dot(normalDir.xy,normalDir.xy)));
                normalDir = normalize(normalDir);
                
                fixed3 lightDir = normalize(f.lightDir);
                fixed3 texColo =  texColor*max(0,dot(normalDir,lightDir)*0.5+0.5);
                fixed3 color = texColo+UNITY_LIGHTMODEL_AMBIENT.rgb;
                return fixed4(color,1);
            }

         
            ENDCG
        }
     
    }
    FallBack "Diffuse"
}
