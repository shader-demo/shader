// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "My/01 shader"{
       Properties{
    _Color("Color",Color) = (1,1,1,1)//颜色
    _Vector("Vector",Vector) =  (3,2,3,1)//向量
    _Int("Int",Int) = 234//int类型
    _Float("Float",Float) = 1.2
    _Range("Range",Range(2,8)) = 6//范围值
    _2D("Texture",2D) = "red"{}//贴图
    _Cube("Cube",Cube) = "white"{}//天空盒
    _3D("Texure",3D) = "black"{}//3D贴图
}
        SubShader{
            Pass{
                CGPROGRAM

                #pragma vertex vert
                #pragma fragment frag
                float4 _Color;
                int _Int;
                //float _Int;这里用float定义_Int也可以
                float _Float;
                float _Range;
                sampler2D _2D;
                samplerCUBE _Cube;
                sampler3D _3D;
                float4 vert(float4 v:POSITION):SV_POSITION{
                  return UnityObjectToClipPos(v);
                    
                }

                fixed4 frag():SV_Target{
                    return fixed4 (_Int,1,1,1);
                }
                ENDCG
                }
        }
          FallBack  "Diffuse"
    
    }