// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "My/02 shader"{
        SubShader{
            Pass{
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                //application to vertex
                struct a2v
                {
                    float4 vertex:POSITION;
                    float3 normal:NORMAL;
                    float4 texcord:TEXCOORD0;
                };

                struct v2f
                {
                    float4 position:SV_POSITION;
                    float3 temp:COLOR1;
                };

                struct f2t
                {
                    fixed4 t:SV_Target;
                };
                v2f vert(a2v v){
                    v2f f;
                    f.position = UnityObjectToClipPos(v.vertex);
                    f.temp = v.normal;
                  return f;
                }

                f2t frag(v2f f){
                    f2t t;
                    t.t = fixed4(f.temp,1);
                    return t;
                }
                ENDCG
                }
        }
    
    }