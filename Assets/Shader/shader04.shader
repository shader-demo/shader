// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "My/04 shader"{
        SubShader{
            Pass{
                CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members normal)
                #include "Lighting.cginc"//引入灯光库
                #pragma vertex vert
                #pragma fragment frag
                //application to vertex
                struct a2v
                {
                    float4 vertex:POSITION;
                    float3 normal:NORMAL;
                };

                struct v2f
                {
                    float4 position:SV_POSITION;
                    float3 color:COLOR0;
                    float3 normal:COLOR1;
                };
                
                v2f vert(a2v v){
                    v2f f;
                    f.position = UnityObjectToClipPos(v.vertex);//顶点转为裁剪空间
                    f.normal = v.normal;
                    return f;
                }

                fixed4 frag(v2f f):SV_Target{
                    fixed3 light_dir = normalize(_WorldSpaceLightPos0.xyz);//归一化灯光方向
                    fixed3 normal_dir = normalize(mul(f.normal,(float3x3)unity_WorldToObject));//先把法线方向转为世界空间，归一化法线方向
                    fixed3 diffuse = _LightColor0.rgb*max(0,dot(light_dir,normal_dir));//点成求cos,然后灯光颜色矩阵相乘获取漫反射光
                    return fixed4(diffuse,1);
                }
                ENDCG
                }
        }
    
    }