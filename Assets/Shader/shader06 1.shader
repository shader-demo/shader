// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "My/06_1 shader"{
        SubShader{
            Pass{
                CGPROGRAM
                #include "Lighting.cginc"//引入灯光库
                #pragma vertex vert
                #pragma fragment frag
                //application to vertex
                struct a2v
                {
                    float4 vertex:POSITION;
                    float3 normal:NORMAL;
                };

                struct v2f
                {
                    float4 position:SV_POSITION;
                    float3 color:COLOR;
                };
                

                v2f vert(a2v v){
                    v2f f;
                    f.position = UnityObjectToClipPos(v.vertex);//顶点转为裁剪空间
                    fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;//环境光
                    fixed3 light_dir = normalize(_WorldSpaceLightPos0.xyz);//归一化灯光方向
                    fixed3 normal_dir = normalize(mul(v.normal,(float3x3)unity_WorldToObject));//先把法线方向转为世界空间，归一化法线方向
                     //高光反射算法,需要把摄像机的方向引入，直射光*pow（max(cosƟ,0),10）,Ɵ是反射光方向和视野方向的夹角
                    fixed3 reflectDir = normalize(reflect(-light_dir,normal_dir));
                    fixed3 cameraDir = normalize(_WorldSpaceCameraPos.xyz-mul(v.vertex,unity_WorldToObject));
                    fixed3 specular = _LightColor0.rgb* pow(max(dot(reflectDir,cameraDir),0),10);
                    // fixed3 specular = _LightColor0.rgb* pow(dot(reflectDir,cameraDir),10);
                    //半兰伯特光照模型
                    fixed3 diffuse = _LightColor0.rgb*max(0,(dot(light_dir,normal_dir)));//点成求cos,然后灯光颜色矩阵相乘获取漫反射光
                    f.color = diffuse;
                    return f;
                }

                fixed4 frag(v2f f):SV_Target{
                    return fixed4(f.color,1);
                }
                ENDCG
                }
        }
    
    }